﻿using UnityEngine;
using System.Collections;

public class k_button_hal_awal : MonoBehaviour {
    public GameObject image_about;
	// Use this for initialization
	void Start () {
        image_about.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void tujuan_scene_loading()
    {
        Application.LoadLevel(5);
    }

    public void about()
    {
        image_about.SetActive(true);
    }

    public void close_about()
    {
        image_about.SetActive(false);
    }
}
