﻿using UnityEngine;
using System.Collections;

public class k_button_story : MonoBehaviour {
    public GameObject image_1;
    public GameObject cerita_1;
    public GameObject back_1;

    public GameObject image_2;
    public GameObject cerita_2;
    public GameObject back_2;

    public GameObject image_3;
    public GameObject cerita_3;
    public GameObject back_3;

    private int total_klik;

	// Use this for initialization
	void Start () {
        total_klik = 0;
        disable_story();
	}
	
	// Update is called once per frame
	void Update () {
        tampil_story();
	}

    public void b_next()
    {
        total_klik++;   
    }

    public void tampil_story()
    {
        if (total_klik == 0)
        {
            image_1.SetActive(true);
            back_1.SetActive(true);
            cerita_1.SetActive(true);
        }
        else if (total_klik == 1)
        {
            image_1.SetActive(false);
            back_1.SetActive(false);
            cerita_1.SetActive(false);

            image_2.SetActive(true);
            back_2.SetActive(true);
            cerita_2.SetActive(true);
        }
        else if (total_klik == 2)
        {
            image_2.SetActive(false);
            back_2.SetActive(false);
            cerita_2.SetActive(false);

            image_3.SetActive(true);
            back_3.SetActive(true);
            cerita_3.SetActive(true);
        }
        else if (total_klik == 3)
        {
            akses_halaman_utama();
        }
        else { total_klik = 4; }
    }

    public void akses_halaman_utama()
    {
        Application.LoadLevel(3);
    }

    public void disable_story()
    {
        image_1.SetActive(false);
        back_1.SetActive(false);
        cerita_1.SetActive(false);

        image_2.SetActive(false);
        back_2.SetActive(false);
        cerita_2.SetActive(false);

        image_3.SetActive(false);
        back_3.SetActive(false);
        cerita_3.SetActive(false);
    }



}
