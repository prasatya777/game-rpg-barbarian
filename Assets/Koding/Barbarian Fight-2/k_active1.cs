﻿using UnityEngine;
using System.Collections;

public class k_active1 : MonoBehaviour {
	// Use this for initialization
    public k_alexandria2 asset_alexandria;
    public k_babon asset_babon;
    public k_musuh_2 asset_musuh_2;
    public k_saka asset_saka;

    public GameObject musuh_2;
    public GameObject alexandria2;
    public GameObject babon;
    public GameObject saka;

    public k_battle_controller1 asset_controller;
	void Start () {
        enable_object();
	}
	
	// Update is called once per frame
	void Update () {
        destroy_musuh_2();
        destroy_karakter();
	}

    public void disable_musuh_2()
    {
        musuh_2.SetActive(false);
    }
    public void disable_karakter()
    {
        alexandria2.SetActive(false);
        babon.SetActive(false);
        saka.SetActive(false);
    }

    public void enable_object()
    {
        musuh_2.SetActive(true);
        alexandria2.SetActive(true);
        babon.SetActive(true);
        saka.SetActive(true);
    }

    public void destroy_musuh_2()
    {
        if (asset_controller.end_musuh_2 == true)
        {
            disable_musuh_2();
        } 
    }

    public void destroy_karakter()
    {
        if (asset_controller.end_karakter == true)
        {
            disable_karakter();
        } 

    }
}
