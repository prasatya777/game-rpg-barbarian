﻿using UnityEngine;
using System.Collections;

public class k_saka : MonoBehaviour {

    public k_babon asset_babon;
    public k_musuh_2 asset_musuh_2;
    public k_alexandria2 k_utama;
    public k_bar_nyawa1 nilai_nyawa;
    public k_bar_power1 nilai_power;
    private AudioSource audioPlayer;  
    public int no_karakter_on;
    public Transform[] path;
    public Transform[] path2;
    public float speed = 3.0f;
    public float reachDist = 1.0f;
    public int currentPoint_saka = 0;
    public int gerakan = 0;

    Animator anim;

    //Variabel Time
    public float waktu;
    public float speed_time = 1;
    public float seconds;
    public float dist;

    //Variabel Balik Badan
    public bool balik;

    //Variabel Indikator Attack
    public int power_attack_saka;
    public bool indikator_power_saka;

    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
        audioPlayer = GetComponent<AudioSource>();
        reset_waktu();
        indikator_power_saka = false;
        power_attack_saka = 0;

    }

    // Update is called once per frame
    void Update()
    {
        no_karakter_on = k_utama.no_karakter_on;
        if (indikator_power_saka == true && gerakan==3)
        { gerakan = 0; }
        if (no_karakter_on == 3 || indikator_power_saka==true)
        {
            if (gerakan == 0) { langkah_depan_saka(); }
            else if (gerakan == 1) { pukulan_saka(); }
            else if (gerakan == 2) { langkah_mundur_saka(); }

        }

    }

    public void langkah_depan_saka()
    {
        dist = Vector3.Distance(path[currentPoint_saka].position, transform.position);
        transform.position = Vector3.MoveTowards(transform.position, path[currentPoint_saka].position, Time.deltaTime * speed);
        anim.SetBool("lari_saka", true);
        //print(dist);
        if (transform.position.x == 6.46f && transform.position.y == -1.44f) { gerakan = 1; }
    }

    public void pukulan_saka()
    {
        waktu += Time.deltaTime * speed_time;
        seconds = waktu % 60;
        if (waktu < 1.3) { anim.SetBool("attack_saka", true); }
        else 
        {
            nilai_power_attack_saka();
            k_utama.nilai_power_attack_alexandria2();
            asset_babon.nilai_power_attack_babon();
            if (indikator_power_saka == true) { asset_musuh_2.life_musuh -= 500 * 2; pengurangan_nyawa_musuh_power_saka(); power_attack_saka = 0; nilai_power.pengurangan_power_saka(); }
            else { asset_musuh_2.life_musuh -= 500; nilai_nyawa.pengurangan_nyawa_musuh_2(); }
            anim.SetBool("attack_saka", false); 
            gerakan = 2; 
            balik_badan(); 
        }
    }

    public void langkah_mundur_saka()
    {
        dist = Vector3.Distance(path2[currentPoint_saka].position, transform.position);
        transform.position = Vector3.MoveTowards(transform.position, path2[currentPoint_saka].position, Time.deltaTime * speed);
        //print(dist);
        if (transform.position.x == -11.44f && transform.position.y == -2.35f)
        {
            balik_badan(); anim.SetBool("lari_saka", false);
            gerakan = 3;
            if (indikator_power_saka != true || k_utama.no_karakter_on==3)
            { k_utama.no_karakter_on = 4; }
            currentPoint_saka = 0;
            if (indikator_power_saka == true) { indikator_power_saka = false; power_attack_saka = 0; nilai_power.pengurangan_power_saka(); }
            if (asset_musuh_2.transform.position.x == 8.31f && asset_musuh_2.transform.position.y == 0.17f) { asset_musuh_2.gerakan = 0; }
            reset_waktu();
        }
    }


    public void balik_badan()
    {
        balik = !balik;
        Vector3 karakter = transform.localScale;
        karakter.x *= -1;
        transform.localScale = karakter;
    }

    public void reset_waktu()
    {
        waktu = 0;
        speed_time = 1;
        seconds = 0;
        dist = 0;
    }
    public void pengurangan_nyawa_musuh_power_saka()
    {
        nilai_nyawa.bar_musuh2.size -= 0.1f;
    }

    public void nilai_power_attack_saka()
    {
        if (power_attack_saka < 100) { power_attack_saka += 100; nilai_power.penambahan_power_saka(); }
    }

    public void palyVoice(AudioClip voice)
    {
        k_set_audio1.playsound(voice, audioPlayer);
    }
}
