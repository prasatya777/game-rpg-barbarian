﻿using UnityEngine;
using System.Collections;
using System.Diagnostics;

public class k_alexandria2 : MonoBehaviour
{

    public k_musuh_2 asset_musuh_2;
    public k_babon asset_babon;
    public k_bar_nyawa1 nilai_nyawa;
    public k_bar_power1 nilai_power;
    private AudioSource audioPlayer;
    public int no_karakter_on = 1;
    public Transform[] path;
    public Transform[] path2;
    public float speed = 3.0f;
    public float reachDist = 1.0f;
    public int currentPoint_alexandria2 = 0;
    public int gerakan = 0;
    
    Animator anim;

    //Variabel Time
    public float waktu;
    public float speed_time = 1;
    public float seconds;
    public float dist;

    //Variabel Balik Badan
    public bool balik;

    //Variabel nyawa Karakter
    public int life_karakter = 10000;

    //Variabel Indikator Attack
    public int power_attack_alexandria2;
    public bool indikator_power_alexandria2;

    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
        audioPlayer = GetComponent<AudioSource>();
        reset_waktu();
        indikator_power_alexandria2 = false;
        power_attack_alexandria2 = 0;
        life_karakter = 10000;
    }

    // Update is called once per frame
    void Update()
    {
        if (indikator_power_alexandria2 == true && gerakan == 3)
            { gerakan = 0; }
        if (no_karakter_on == 1 || indikator_power_alexandria2==true)
        {
            if (gerakan == 0) { langkah_depan_alexandria2(); }
            else if (gerakan == 1) { pukulan_alexandria2(); }
            else if (gerakan == 2) { langkah_mundur_alexandria2();}

        }

    }

    public void langkah_depan_alexandria2()
    {
        dist = Vector3.Distance(path[currentPoint_alexandria2].position, transform.position);
        transform.position = Vector3.MoveTowards(transform.position, path[currentPoint_alexandria2].position, Time.deltaTime * speed);
        anim.SetBool("lari_alexandria", true);
        //print(dist);
        if (transform.position.x == 6.46f && transform.position.y == -1.44f) { gerakan = 1; }
    }

    public void pukulan_alexandria2()
    {
        waktu += Time.deltaTime * speed_time;
        seconds = waktu % 60;
        if (waktu < 1.3) { anim.SetBool("attack_alexandria", true);}
        else 
        {
            nilai_power_attack_alexandria2();
            if (indikator_power_alexandria2 == true) { asset_musuh_2.life_musuh -= 500 * 2; pengurangan_nyawa_musuh_power_alexandria2(); power_attack_alexandria2 = 0; nilai_power.pengurangan_power_alexandria2(); }
            else { asset_musuh_2.life_musuh -= 500; nilai_nyawa.pengurangan_nyawa_musuh_2(); }
            anim.SetBool("attack_alexandria", false); 
            gerakan = 2; balik_badan(); 
        }
    }

    public void langkah_mundur_alexandria2()
    {
        dist = Vector3.Distance(path2[currentPoint_alexandria2].position, transform.position);
        transform.position = Vector3.MoveTowards(transform.position, path2[currentPoint_alexandria2].position, Time.deltaTime * speed);
        //print(dist);
        if (transform.position.x == -6.84f && transform.position.y == -1.46f) 
        { 
            balik_badan(); 
            anim.SetBool("lari_alexandria", false); 
            gerakan = 3;
            if (indikator_power_alexandria2 != true||no_karakter_on == 1)
                { no_karakter_on = 2; }
            if (indikator_power_alexandria2 == true) { indikator_power_alexandria2 = false; power_attack_alexandria2 = 0; nilai_power.pengurangan_power_alexandria2(); }
            if (asset_babon.transform.position.x == -11.89f && asset_babon.transform.position.y == 2.02f) { asset_babon.gerakan = 0; }
            currentPoint_alexandria2 = 0;
            reset_waktu();
            
        }
    }


    public void balik_badan()
    {
        balik = !balik;
        Vector3 karakter = transform.localScale;
        karakter.x *= -1;
        transform.localScale=karakter;
    }

    public void reset_waktu()
    {
    waktu = 0;
    speed_time = 1;
    seconds = 0;
    dist = 0;
    }

    public void pengurangan_nyawa_musuh_power_alexandria2()
    {
        nilai_nyawa.bar_musuh2.size -= 0.1f;
    }

    public void nilai_power_attack_alexandria2() {
        if (power_attack_alexandria2 < 100) { power_attack_alexandria2 += 100; nilai_power.penambahan_power_alexandria2(); }
    }

    public void palyVoice(AudioClip voice)
    {
        k_set_audio1.playsound(voice, audioPlayer);
    }
}
