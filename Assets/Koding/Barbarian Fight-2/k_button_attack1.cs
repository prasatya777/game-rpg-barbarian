﻿using UnityEngine;
using System.Collections;

public class k_button_attack1 : MonoBehaviour {
    public k_alexandria2 asset_alexandria2;
    public k_babon asset_babon;
    public k_saka asset_saka;
    public k_bar_power1 nilai_power;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void button_attack1_alexandria2()
    {
        if (asset_alexandria2.power_attack_alexandria2 >= 100) { asset_alexandria2.indikator_power_alexandria2 = true; nilai_power.pengurangan_power_alexandria2(); }
    }

    public void button_attack1_babon()
    {
        if (asset_babon.power_attack_babon >= 100) { asset_babon.indikator_power_babon = true; nilai_power.pengurangan_power_babon(); }
    }

    public void button_attack1_saka()
    {
        if (asset_saka.power_attack_saka >= 100) { asset_saka.indikator_power_saka = true; nilai_power.pengurangan_power_saka(); }
    }

}
