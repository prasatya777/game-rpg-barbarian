﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class k_timer_battle1 : MonoBehaviour {
    public int roundtime = 200;
    private float lasttimeupdate = 0;
    public Text timer_text;

	// Use this for initialization
	void Start () {
        roundtime = 200;
	}
	
	// Update is called once per frame
	void Update () {
            hitungan_mundur();
            timer_text.text = roundtime.ToString();
	}

    public void hitungan_mundur() {
        if (roundtime > 0 && Time.time - lasttimeupdate > 1)
        {
            roundtime--;
            lasttimeupdate = Time.time;
        }
    }
}
