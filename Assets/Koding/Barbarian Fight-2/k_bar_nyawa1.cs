﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class k_bar_nyawa1 : MonoBehaviour {
    public Scrollbar bar_musuh2;
    public Scrollbar bar_alexandria2;
    public Scrollbar bar_babon;
    public Scrollbar bar_saka;

	// Use this for initialization
	void Start () {
        bar_alexandria2.size = 1f;
        bar_saka.size = 1f;
        bar_babon.size = 1f;
	}
	
	// Update is called once per frame
	void Update () {

	}

    public void pengurangan_nyawa_musuh_2 ()
    {
        bar_musuh2.size -= 0.05f;
    }

    public void pengurangan_nyawa_karakter()
    {
        bar_alexandria2.size -= 0.1f;
        bar_babon.size -= 0.1f;
        bar_saka.size -= 0.1f;
    }
}
