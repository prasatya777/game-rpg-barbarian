﻿using UnityEngine;
using System.Collections;
using System.Diagnostics;

public class k_babon : MonoBehaviour {
    public k_musuh_2 asset_musuh_2;
    public k_saka asset_saka;
    public k_alexandria2 k_utama;
    public k_bar_nyawa1 nilai_nyawa;
    public k_bar_power1 nilai_power;
    private AudioSource audioPlayer;
    public int no_karakter_on;
    public Transform[] path;
    public Transform[] path2;
    public float speed = 3.0f;
    public float reachDist = 1.0f;
    public int currentPoint_babon = 0;
    public int gerakan;

    Animator anim;

    //Variabel Time
    public float waktu;
    public float speed_time = 1;
    public float seconds;
    public float dist;

    //Variabel Balik Badan
    public bool balik;

    //Variabel Indikator Attack
    public int power_attack_babon;
    public bool indikator_power_babon;

    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
        audioPlayer = GetComponent<AudioSource>();
        reset_waktu();
        indikator_power_babon = false;
        power_attack_babon = 0;
    }

    // Update is called once per frame
    void Update()
    {
        no_karakter_on = k_utama.no_karakter_on;
        if (indikator_power_babon == true && gerakan == 3)
        { gerakan = 0; }
        if (no_karakter_on == 2 || indikator_power_babon==true)
        {
            if (gerakan == 0) { langkah_depan_babon(); }
            else if (gerakan == 1) { pukulan_babon(); }
            else if (gerakan == 2) { langkah_mundur_babon(); }

        }

    }

    public void langkah_depan_babon()
    {
        dist = Vector3.Distance(path[currentPoint_babon].position, transform.position);
        transform.position = Vector3.MoveTowards(transform.position, path[currentPoint_babon].position, Time.deltaTime * speed);
        anim.SetBool("lari_babon", true);
        //print(dist);
        if (transform.position.x == 6.46f && transform.position.y == -1.44f) { gerakan = 1; }
    }

    public void pukulan_babon()
    {
        waktu += Time.deltaTime * speed_time;
        seconds = waktu % 60;
        if (waktu < 1.3) { anim.SetBool("attack_babon", true); }
        else 
        {
            nilai_power_attack_babon();
            k_utama.nilai_power_attack_alexandria2();
            if (indikator_power_babon == true) { asset_musuh_2.life_musuh -= 500 * 2; pengurangan_nyawa_musuh_power_babon(); power_attack_babon = 0; nilai_power.pengurangan_power_babon(); }
            else { asset_musuh_2.life_musuh -= 500; nilai_nyawa.pengurangan_nyawa_musuh_2(); }
            anim.SetBool("attack_babon", false); 
            gerakan = 2;
            balik_badan();
            
        }
    }

    public void langkah_mundur_babon()
    {
        dist = Vector3.Distance(path2[currentPoint_babon].position, transform.position);
        transform.position = Vector3.MoveTowards(transform.position, path2[currentPoint_babon].position, Time.deltaTime * speed);
        //print(dist);
        if (transform.position.x == -11.89f && transform.position.y == 2.02f) 
        { 
            balik_badan(); anim.SetBool("lari_babon", false); 
            gerakan = 3;
            if (indikator_power_babon != true || k_utama.no_karakter_on==2)
            { k_utama.no_karakter_on = 3; }
            currentPoint_babon = 0;
            if (indikator_power_babon == true) { indikator_power_babon = false; power_attack_babon = 0; nilai_power.pengurangan_power_babon(); }
            if (asset_saka.transform.position.x == -11.44f && asset_saka.transform.position.y == -2.35f) { asset_saka.gerakan = 0; }
            reset_waktu();
            
        }
    }

    public void balik_badan()
    {
        balik = !balik;
        Vector3 karakter = transform.localScale;
        karakter.x *= -1;
        transform.localScale = karakter;
    }

    public void reset_waktu()
    {
        waktu = 0;
        speed_time = 1;
        seconds = 0;
        dist = 0;
    }

    public void pengurangan_nyawa_musuh_power_babon()
    {
        nilai_nyawa.bar_musuh2.size -= 0.1f;
    }

    public void nilai_power_attack_babon()
    {
        if (power_attack_babon < 100) { power_attack_babon += 100; nilai_power.penambahan_power_babon(); }
        else { }
    }

    public void palyVoice(AudioClip voice)
    {
        k_set_audio.playsound(voice, audioPlayer);
    }
}
