﻿using UnityEngine;
using System.Collections;

public class k_musuh_2 : MonoBehaviour {
    public k_saka asset_saka;
    public k_babon asset_babon;
    public k_alexandria2 k_utama;
    public k_bar_nyawa1 nilai_nyawa;
    public k_bar_power1 nilai_power;
    private AudioSource audioPlayer; 
    public int no_karakter_on;
    public Transform[] path;
    public Transform[] path2;
    public float speed = 3.0f;
    public float reachDist = 1.0f;
    public int currentPoint_musuh_2 = 0;
    public int gerakan = 0;

    Animator anim;

    //Variabel Time
    public float waktu;
    public float speed_time = 1;
    public float seconds;
    public float dist;

    //Variabel Balik Badan
    public bool balik;

    //Variabel nyawa Musuh
    public int life_musuh = 10000;

    //Variabel Indikator Attack
    public int power_attack_musuh_2;
    public bool indikator_power_musuh_2;

    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
        audioPlayer = GetComponent<AudioSource>();
        reset_waktu();
        indikator_power_musuh_2 = false;
        power_attack_musuh_2 = 0;
        life_musuh = 10000;
    }

    // Update is called once per frame
    void Update()
    {
        no_karakter_on = k_utama.no_karakter_on;
        //if (indikator_power_musuh_2 == true && gerakan == 3)
        //{ gerakan = 0; }
        if (no_karakter_on == 4)
        {
            if (gerakan == 0) { langkah_depan_musuh_2(); }
            else if (gerakan == 1) { pukulan_musuh_2(); }
            else if (gerakan == 2) { langkah_mundur_musuh_2(); }

        }

    }

    public void langkah_depan_musuh_2()
    {
        dist = Vector3.Distance(path[currentPoint_musuh_2].position, transform.position);
        transform.position = Vector3.MoveTowards(transform.position, path[currentPoint_musuh_2].position, Time.deltaTime * speed);
        anim.SetBool("lari_musuh2", true);
        //print(dist);
        if (transform.position.x == -3.95f && transform.position.y == -0.75f) { gerakan = 1; }
    }

    public void pukulan_musuh_2()
    {
        cek_nilai_power_attack_musuh_2();
        waktu += Time.deltaTime * speed_time;
        seconds = waktu % 60;
        if (waktu < 1.3) { anim.SetBool("attack_musuh2", true); }
        else 
        {
            nilai_power_attack_musuh_2();
            asset_babon.nilai_power_attack_babon();
            asset_saka.nilai_power_attack_saka();
            k_utama.nilai_power_attack_alexandria2();

            if (indikator_power_musuh_2 == true) { k_utama.life_karakter -= 2000 * 2; pengurangan_nyawa_karakter(); nilai_power.pengurangan_power_musuh_2(); power_attack_musuh_2 = 0; nilai_power.pengurangan_power_musuh_2(); }
            else { k_utama.life_karakter -= 1000; nilai_nyawa.pengurangan_nyawa_karakter(); }
            anim.SetBool("attack_musuh2", false); 
            gerakan = 2; balik_badan();
        }
    }

    public void langkah_mundur_musuh_2()
    {
        dist = Vector3.Distance(path2[currentPoint_musuh_2].position, transform.position);
        transform.position = Vector3.MoveTowards(transform.position, path2[currentPoint_musuh_2].position, Time.deltaTime * speed);
        //print(dist);
        if (transform.position.x == 8.31f && transform.position.y == 0.17f)
        {
            balik_badan(); anim.SetBool("lari_musuh2", false);
            gerakan = 3;
            if (indikator_power_musuh_2 != true || k_utama.no_karakter_on==4)
            { k_utama.no_karakter_on = 1; }
            currentPoint_musuh_2 = 0;
            if (indikator_power_musuh_2 == true) { indikator_power_musuh_2 = false; power_attack_musuh_2 = 0; nilai_power.pengurangan_power_musuh_2(); }
            if (k_utama.transform.position.x == -6.84f && k_utama.transform.position.y == -1.46f) { k_utama.gerakan = 0; }
            reset_waktu();
            indikator_power_musuh_2 = false;
        }
    }


    public void balik_badan()
    {
        balik = !balik;
        Vector3 karakter = transform.localScale;
        karakter.x *= -1;
        transform.localScale = karakter;
    }

    public void reset_waktu()
    {
        waktu = 0;
        speed_time = 1;
        seconds = 0;
        dist = 0;
    }

    public void pengurangan_nyawa_karakter()
    {
        nilai_nyawa.bar_alexandria2.size -= 0.4f;
        nilai_nyawa.bar_babon.size -= 0.4f;
        nilai_nyawa.bar_saka.size -= 0.4f;
    }

    public void nilai_power_attack_musuh_2()
    {
        if (power_attack_musuh_2 < 100) { power_attack_musuh_2 += 100; nilai_power.penambahan_power_musuh_2(); }
    }

    public void cek_nilai_power_attack_musuh_2()
    {
        if (power_attack_musuh_2 >= 100) { indikator_power_musuh_2 = true;}
    }

    public void palyVoice(AudioClip voice)
    {
        k_set_audio1.playsound(voice, audioPlayer);
    }
}
