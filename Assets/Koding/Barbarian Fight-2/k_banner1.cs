﻿using UnityEngine;
using System.Collections;

public class k_banner1 : MonoBehaviour {
    public Animator animator;
    private bool animating;

	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
 
	}

    public void banner1_fight(){
        animating = true;
        animator.SetTrigger("t_fight");
    }

    public void banner1_win()
    {
        animating = true;
        animator.SetTrigger("t_win");
    }

    public void banner1_lose()
    {
        animating = true;
        animator.SetTrigger("t_lose");
    }

    public void akhir_animasi() {
        animating = false;
    }

    public void akhir_animasi_win()
    {
        animating = false;
    }

    public void akhir_animasi_lose()
    {
        animating = false;
    }

    public bool isAnimating
    {
        get
        {
            return animating;
        }
    }
}
