﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]

public class k_battle_controller1 : MonoBehaviour {
    public k_banner1 banner1;
    public k_alexandria2 asset_alexandria2;
    public k_saka asset_saka;
    public k_musuh_2 asset_musuh_2;
    public k_babon asset_babon;
    public k_timer_battle1 timer;
    public Canvas asset_pop_up1;
    public Button button_pause;
    public Button button_alexandria2;
    public Button button_babon;
    public Button button_saka;
    public bool end_musuh_2;
    public bool end_karakter;

    public AudioSource music_main_camera;
    public AudioClip backsound;
    public bool endgame;

	// Use this for initialization
	void Start () {
        k_set_audio1.playsound(backsound, music_main_camera);
        banner1.banner1_fight();
        endgame = false;
        asset_alexandria2.enabled = false;
        asset_saka.enabled = false;
        asset_babon.enabled = false;
        asset_musuh_2.enabled = false;
        timer.enabled = false;
        asset_pop_up1.enabled = false;
        disable_button();
        end_karakter = false;
        end_musuh_2 = false;
	}
	
	// Update is called once per frame
	void Update () {
        game_mulai();
        game_berakhir();
	}

    public void game_mulai()
    {
        if (!banner1.isAnimating && endgame == false)
        {
            asset_alexandria2.enabled = true;
            asset_saka.enabled = true;
            asset_babon.enabled = true;
            asset_musuh_2.enabled = true;
            timer.enabled = true;
            enable_button();
        }
    }

    public void game_berakhir()
    {
        if (asset_musuh_2.life_musuh <= 0) 
        {
         end_musuh_2 = true;
         berhenti();
         if (endgame == false){banner1.banner1_win();}
         endgame = true;
         if (!banner1.isAnimating) { scene_map(); }
        }
        else if (asset_alexandria2.life_karakter <= 0)
        {
            end_karakter = true;
            berhenti();
            if (endgame == false){banner1.banner1_lose();}
            endgame = true;
            if (!banner1.isAnimating) { scene_map(); }
        }
    }

    public void waktu_habis()
    {
        if (timer.roundtime <= 0)
        { game_berakhir(); }
    }

    public void berhenti()
    {
        asset_alexandria2.enabled = false;
        asset_saka.enabled = false;
        asset_babon.enabled = false;
        asset_musuh_2.enabled = false;
        timer.enabled = false;
        disable_button();
        
    }

    public void disable_button() 
    {
        button_pause.enabled = false;
        button_alexandria2.enabled = false;
        button_babon.enabled = false;
        button_saka.enabled = false;
    }

    public void enable_button()
    {
        button_pause.enabled = true;
        button_alexandria2.enabled = true;
        button_babon.enabled = true;
        button_saka.enabled = true;
    }

    public void scene_map()
    {
        Application.LoadLevel(0);
    }

}
