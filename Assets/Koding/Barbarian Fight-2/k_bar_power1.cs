﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class k_bar_power1 : MonoBehaviour {
    public Scrollbar bar_power1_musuh_2;
    public Scrollbar bar_power1_alexandria2;
    public Scrollbar bar_power1_babon;
    public Scrollbar bar_power1_saka;

	// Use this for initialization
	void Start () {
        bar_power1_musuh_2.size = 0f;
        bar_power1_alexandria2.size = 0f;
        bar_power1_saka.size = 0f;
        bar_power1_babon.size = 0f;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void pengurangan_power_musuh_2()
    {
        bar_power1_musuh_2.size -= 1f;
    }

    public void pengurangan_power_alexandria2()
    {
        bar_power1_alexandria2.size -= 1f;
    }

    public void pengurangan_power_babon()
    {
        bar_power1_babon.size -= 1f;
    }

    public void pengurangan_power_saka()
    {
        bar_power1_saka.size -= 1f;
    }

//penambahan power ---------------------------------------------------
    public void penambahan_power_musuh_2()
    {
        bar_power1_musuh_2.size += 1f;
    }
    
    public void penambahan_power_alexandria2()
    {
        bar_power1_alexandria2.size += 1f;
    }

    public void penambahan_power_babon()
    {
        bar_power1_babon.size += 1f;
    }

    public void penambahan_power_saka()
    {
        bar_power1_saka.size += 1f;
    }
}
