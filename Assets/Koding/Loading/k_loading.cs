﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class k_loading : MonoBehaviour {
    public Scrollbar bar_loading;

    public float waktu;
    public float speed_time = 1;
    public float seconds;
    public float nilai_acuan;

	// Use this for initialization
	void Start () {
        bar_loading.size = 0f;
        reset_waktu();
        nilai_acuan = 2;
	}
	
	// Update is called once per frame
	void Update () {
        loading_time();
        loading_bar();
        tujuan_scene_story();
	}

    public void loading_time()
    {
        waktu += Time.deltaTime * speed_time;
        seconds = waktu % 60;
    }

    public void loading_bar()
    {

        if (waktu >= nilai_acuan) { bar_loading.size += 0.1f; nilai_acuan += 2; }
    }

    public void tujuan_scene_story()
    {
        if (waktu >= 20)
        {
            Application.LoadLevel(4);
        }
    }

    public void reset_waktu()
    {
        waktu = 0;
        speed_time = 1;
        seconds = 0;
    }
}
