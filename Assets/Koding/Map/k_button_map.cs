﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]

public class k_button_map : MonoBehaviour {
    public Button b_level_2;

    public k_cotroller_scene asset_controller_scene;
	// Use this for initialization
	void Start () {
        b_level_2.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
        aktif_b_level_2();
	}

    public void akses_scene_level_1()
    {
        Application.LoadLevel(1);
    }

    public void akses_scene_level_2()
    {
        Application.LoadLevel(2);
    }

    public void akses_scene_halaman_utama()
    {
        Application.LoadLevel(3);
    }

    public void aktif_b_level_2()
    {
        if (asset_controller_scene.nilai_level_1 == true)
        {
            b_level_2.enabled = true;
        }
    }

}
