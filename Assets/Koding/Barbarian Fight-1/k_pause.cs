﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]

public class k_pause : MonoBehaviour {

    public k_pop_up asset_pop_up;
    public bool nilai_pause;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void pause_game()
    {
        Time.timeScale = 0;
        asset_pop_up.tampil_pop_up();
    }

}
