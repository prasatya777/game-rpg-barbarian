﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class k_bar_power : MonoBehaviour {
    public Scrollbar bar_power_musuh_1;
    public Scrollbar bar_power_alexandria;
    public Scrollbar bar_power_bambang;
    public Scrollbar bar_power_amana;

	// Use this for initialization
	void Start () {
        bar_power_musuh_1.size = 0f;
        bar_power_alexandria.size = 0f;
        bar_power_amana.size = 0f;
        bar_power_bambang.size = 0f;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void pengurangan_power_musuh_1()
    {
        bar_power_musuh_1.size -= 1f;
    }

    public void pengurangan_power_alexandria()
    {
        bar_power_alexandria.size -= 1f;
    }

    public void pengurangan_power_bambang()
    {
        bar_power_bambang.size -= 1f;
    }

    public void pengurangan_power_amana()
    {
        bar_power_amana.size -= 1f;
    }

//penambahan power ---------------------------------------------------
    public void penambahan_power_musuh_1()
    {
        bar_power_musuh_1.size += 1f;
    }
    
    public void penambahan_power_alexandria()
    {
        bar_power_alexandria.size += 0.5f;
    }

    public void penambahan_power_bambang()
    {
        bar_power_bambang.size += 0.5f;
    }

    public void penambahan_power_amana()
    {
        bar_power_amana.size += 0.5f;
    }
}
