﻿using UnityEngine;
using System.Collections;

public class k_set_audio {
    public static void playsound(AudioClip clip, AudioSource audio)
    {
        audio.Stop();
        audio.clip = clip;
        audio.loop = false;
        audio.time = 0;
        audio.Play();
    }
}
