﻿using UnityEngine;
using System.Collections;

public class k_active : MonoBehaviour {
	// Use this for initialization
    public k_alexandria asset_alexandria;
    public k_amana asset_amana;
    public k_musuh_1 asset_musuh_1;
    public k_bambang asset_bambang;

    public GameObject musuh_1;
    public GameObject alexandria;
    public GameObject bambang;
    public GameObject amana;

    public k_battle_controller asset_controller;
	void Start () {
        enable_object();
	}
	
	// Update is called once per frame
	void Update () {
        destroy_musuh_1();
        destroy_karakter();
	}

    public void disable_musuh_1()
    {
        musuh_1.SetActive(false);
    }
    public void disable_karakter()
    {
        alexandria.SetActive(false);
        bambang.SetActive(false);
        amana.SetActive(false);
    }

    public void enable_object()
    {
        musuh_1.SetActive(true);
        alexandria.SetActive(true);
        bambang.SetActive(true);
        amana.SetActive(true);
    }

    public void destroy_musuh_1()
    {
        if (asset_controller.end_musuh_1 == true)
        {
            disable_musuh_1();
        } 
    }

    public void destroy_karakter()
    {
        if (asset_controller.end_karakter == true)
        {
            disable_karakter();
        } 

    }
}
