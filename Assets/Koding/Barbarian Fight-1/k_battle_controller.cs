﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]

public class k_battle_controller : MonoBehaviour {
    public k_banner banner;
    public k_alexandria asset_alexandria;
    public k_amana asset_amana;
    public k_musuh_1 asset_musuh_1;
    public k_bambang asset_bambang;
    public k_timer_battle timer;
    public k_cotroller_scene asset_controller_scene;

    public Canvas asset_pop_up;
    public Button button_pause;
    public Button button_alexandria;
    public Button button_bambang;
    public Button button_amana;
    public bool end_musuh_1;
    public bool end_karakter;

    public AudioSource music_main_camera;
    public AudioClip backsound;
    public bool endgame;

	// Use this for initialization
	void Start () {
        k_set_audio.playsound(backsound, music_main_camera);
        banner.banner_fight();
        endgame = false;
        asset_alexandria.enabled = false;
        asset_amana.enabled = false;
        asset_bambang.enabled = false;
        asset_musuh_1.enabled = false;
        timer.enabled = false;
        asset_pop_up.enabled = false;
        disable_button();
        end_karakter = false;
        end_musuh_1 = false;
	}
	
	// Update is called once per frame
	void Update () {
        game_mulai();
        game_berakhir();
	}

    public void game_mulai()
    {
        if (!banner.isAnimating && endgame == false)
        {
            asset_alexandria.enabled = true;
            asset_amana.enabled = true;
            asset_bambang.enabled = true;
            asset_musuh_1.enabled = true;
            timer.enabled = true;
            enable_button();
        }
    }

    public void game_berakhir()
    {
        if (asset_musuh_1.life_musuh <= 0) 
        {
         asset_controller_scene.nilai_level_1 = true;
         end_musuh_1 = true;
         berhenti();
         if (endgame == false){ banner.banner_win(); }
         endgame = true;
         if (!banner.isAnimating) {scene_map(); }
        }
        else if (asset_alexandria.life_karakter <= 0)
        {
            end_karakter = true;
            berhenti();
            if (endgame == false) { banner.banner_lose(); }
            endgame = true;
            if (!banner.isAnimating) {scene_map(); }
        }
    }

    public void waktu_habis()
    {
        if (timer.roundtime <= 0)
        { game_berakhir(); }
    }

    public void berhenti()
    {
        asset_alexandria.enabled = false;
        asset_amana.enabled = false;
        asset_bambang.enabled = false;
        asset_musuh_1.enabled = false;
        timer.enabled = false;
        disable_button();
        
    }

    public void disable_button() 
    {
        button_pause.enabled = false;
        button_alexandria.enabled = false;
        button_bambang.enabled = false;
        button_amana.enabled = false;
    }

    public void enable_button()
    {
        button_pause.enabled = true;
        button_alexandria.enabled = true;
        button_bambang.enabled = true;
        button_amana.enabled = true;
    }

    public void scene_map()
    {
        Application.LoadLevel(0);
    }

}
