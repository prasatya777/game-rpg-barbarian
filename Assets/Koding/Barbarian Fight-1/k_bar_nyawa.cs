﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class k_bar_nyawa : MonoBehaviour {
    public Scrollbar bar_musuh1;
    public Scrollbar bar_alexandria;
    public Scrollbar bar_bambang;
    public Scrollbar bar_amana;

	// Use this for initialization
	void Start () {
        bar_alexandria.size = 1f;
        bar_amana.size = 1f;
        bar_bambang.size = 1f;
	}
	
	// Update is called once per frame
	void Update () {

	}

    public void pengurangan_nyawa_musuh_1 ()
    {
        bar_musuh1.size -= 0.1f;
    }

    public void pengurangan_nyawa_karakter()
    {
        bar_alexandria.size -= 0.3f;
        bar_bambang.size -= 0.3f;
        bar_amana.size -= 0.3f;
    }
}
