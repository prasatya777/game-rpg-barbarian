﻿using UnityEngine;
using System.Collections;

public class k_button_attack : MonoBehaviour {
    public k_alexandria asset_alexandria;
    public k_bambang asset_bambang;
    public k_amana asset_amana;
    public k_bar_power nilai_power;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void button_attack_alexandria()
    {
        if (asset_alexandria.power_attack_alexandria >= 100) { asset_alexandria.indikator_power_alexandria = true; nilai_power.pengurangan_power_alexandria(); }
    }

    public void button_attack_bambang()
    {
        if (asset_bambang.power_attack_bambang >= 100) { asset_bambang.indikator_power_bambang = true; nilai_power.pengurangan_power_bambang(); }
    }

    public void button_attack_amana()
    {
        if (asset_amana.power_attack_amana >= 100) { asset_amana.indikator_power_amana = true; nilai_power.pengurangan_power_amana(); }
    }

}
