﻿using UnityEngine;
using System.Collections;
using System.Diagnostics;

public class k_alexandria : MonoBehaviour
{

    public k_musuh_1 asset_musuh_1;
    public k_bambang asset_bambang;
    public k_bar_nyawa nilai_nyawa;
    public k_bar_power nilai_power;
    private AudioSource audioPlayer;
    public int no_karakter_on = 1;
    public Transform[] path;
    public Transform[] path2;
    public float speed = 3.0f;
    public float reachDist = 1.0f;
    public int currentPoint_alexandria = 0;
    public int gerakan = 0;
    
    Animator anim;

    //Variabel Time
    public float waktu;
    public float speed_time = 1;
    public float seconds;
    public float dist;

    //Variabel Balik Badan
    public bool balik;

    //Variabel nyawa Karakter
    public int life_karakter = 10000;

    //Variabel Indikator Attack
    public int power_attack_alexandria;
    public bool indikator_power_alexandria;

    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
        audioPlayer = GetComponent<AudioSource>();
        reset_waktu();
        indikator_power_alexandria = false;
        power_attack_alexandria = 0;
        life_karakter = 10000;
    }

    // Update is called once per frame
    void Update()
    {
        if (indikator_power_alexandria == true && gerakan == 3)
            { gerakan = 0; }
        if (no_karakter_on == 1 || indikator_power_alexandria==true)
        {
            if (gerakan == 0) { langkah_depan_alexandria(); }
            else if (gerakan == 1) { pukulan_alexandria(); }
            else if (gerakan == 2) { langkah_mundur_alexandria();}

        }

    }

    public void langkah_depan_alexandria()
    {
        dist = Vector3.Distance(path[currentPoint_alexandria].position, transform.position);
        transform.position = Vector3.MoveTowards(transform.position, path[currentPoint_alexandria].position, Time.deltaTime * speed);
        anim.SetBool("lari_alexandria", true);
        //print(dist);
        if (transform.position.x == 6.46f && transform.position.y == -1.44f) { gerakan = 1; }
    }

    public void pukulan_alexandria()
    {
        waktu += Time.deltaTime * speed_time;
        seconds = waktu % 60;
        if (waktu < 1.3) { anim.SetBool("attack_alexandria", true);}
        else 
        {
            nilai_power_attack_alexandria();
            if (indikator_power_alexandria == true) { asset_musuh_1.life_musuh -= 1000 * 2; pengurangan_nyawa_musuh_power_alexandria(); power_attack_alexandria = 0; nilai_power.pengurangan_power_alexandria(); }
            else { asset_musuh_1.life_musuh -= 1000; nilai_nyawa.pengurangan_nyawa_musuh_1(); }
            anim.SetBool("attack_alexandria", false); 
            gerakan = 2; balik_badan(); 
        }
    }

    public void langkah_mundur_alexandria()
    {
        dist = Vector3.Distance(path2[currentPoint_alexandria].position, transform.position);
        transform.position = Vector3.MoveTowards(transform.position, path2[currentPoint_alexandria].position, Time.deltaTime * speed);
        //print(dist);
        if (transform.position.x == -6.84f && transform.position.y == -1.46f) 
        { 
            balik_badan(); 
            anim.SetBool("lari_alexandria", false); 
            gerakan = 3;
            if (indikator_power_alexandria != true||no_karakter_on == 1)
                { no_karakter_on = 2; }

            if (indikator_power_alexandria == true) { indikator_power_alexandria = false; power_attack_alexandria = 0; nilai_power.pengurangan_power_alexandria(); }

            if (asset_bambang.transform.position.x == -11.89f && asset_bambang.transform.position.y == 2.02f) { asset_bambang.gerakan = 0; }
            currentPoint_alexandria = 0;
            reset_waktu();
            
        }
    }


    public void balik_badan()
    {
        balik = !balik;
        Vector3 karakter = transform.localScale;
        karakter.x *= -1;
        transform.localScale=karakter;
    }

    public void reset_waktu()
    {
    waktu = 0;
    speed_time = 1;
    seconds = 0;
    dist = 0;
    }

    public void pengurangan_nyawa_musuh_power_alexandria()
    {
        nilai_nyawa.bar_musuh1.size -= 0.2f;
    }

    public void nilai_power_attack_alexandria() {
        if (power_attack_alexandria < 100) { power_attack_alexandria += 50; nilai_power.penambahan_power_alexandria(); }
    }

    public void palyVoice(AudioClip voice)
    {
        k_set_audio.playsound(voice, audioPlayer);
    }
}
