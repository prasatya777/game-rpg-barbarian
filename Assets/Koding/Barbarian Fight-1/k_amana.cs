﻿using UnityEngine;
using System.Collections;

public class k_amana : MonoBehaviour {

    public k_bambang asset_bambang;
    public k_musuh_1 asset_musuh_1;
    public k_alexandria k_utama;
    public k_bar_nyawa nilai_nyawa;
    public k_bar_power nilai_power;
    private AudioSource audioPlayer;  
    public int no_karakter_on;
    public Transform[] path;
    public Transform[] path2;
    public float speed = 3.0f;
    public float reachDist = 1.0f;
    public int currentPoint_amana = 0;
    public int gerakan = 0;

    Animator anim;

    //Variabel Time
    public float waktu;
    public float speed_time = 1;
    public float seconds;
    public float dist;

    //Variabel Balik Badan
    public bool balik;

    //Variabel Indikator Attack
    public int power_attack_amana;
    public bool indikator_power_amana;

    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
        audioPlayer = GetComponent<AudioSource>();
        reset_waktu();
        indikator_power_amana = false;
        power_attack_amana = 0;

    }

    // Update is called once per frame
    void Update()
    {
        no_karakter_on = k_utama.no_karakter_on;
        if (indikator_power_amana == true && gerakan==3)
        { gerakan = 0; }
        if (no_karakter_on == 3 || indikator_power_amana==true)
        {
            if (gerakan == 0) { langkah_depan_amana(); }
            else if (gerakan == 1) { pukulan_amana(); }
            else if (gerakan == 2) { langkah_mundur_amana(); }

        }

    }

    public void langkah_depan_amana()
    {
        dist = Vector3.Distance(path[currentPoint_amana].position, transform.position);
        transform.position = Vector3.MoveTowards(transform.position, path[currentPoint_amana].position, Time.deltaTime * speed);
        anim.SetBool("lari_amana", true);
        //print(dist);
        if (transform.position.x == 6.46f && transform.position.y == -1.44f) { gerakan = 1; }
    }

    public void pukulan_amana()
    {
        waktu += Time.deltaTime * speed_time;
        seconds = waktu % 60;
        if (waktu < 1.3) { anim.SetBool("attack_amana", true); }
        else 
        {
            nilai_power_attack_amana();
            k_utama.nilai_power_attack_alexandria();
            asset_bambang.nilai_power_attack_bambang();

            if (indikator_power_amana == true) { asset_musuh_1.life_musuh -= 1000 * 2; pengurangan_nyawa_musuh_power_amana(); power_attack_amana = 0; nilai_power.pengurangan_power_amana(); }
            else { asset_musuh_1.life_musuh -= 1000; nilai_nyawa.pengurangan_nyawa_musuh_1(); }

            anim.SetBool("attack_amana", false); 
            gerakan = 2; 
            balik_badan(); 
        }
    }

    public void langkah_mundur_amana()
    {
        dist = Vector3.Distance(path2[currentPoint_amana].position, transform.position);
        transform.position = Vector3.MoveTowards(transform.position, path2[currentPoint_amana].position, Time.deltaTime * speed);
        //print(dist);
        if (transform.position.x == -11.44f && transform.position.y == -2.35f)
        {
            balik_badan(); anim.SetBool("lari_amana", false);
            gerakan = 3;
            if (indikator_power_amana != true || k_utama.no_karakter_on==3)
            { k_utama.no_karakter_on = 4; }
            currentPoint_amana = 0;
            if (indikator_power_amana == true) { indikator_power_amana = false; power_attack_amana = 0; nilai_power.pengurangan_power_amana(); }
            if (asset_musuh_1.transform.position.x == 8.31f && asset_musuh_1.transform.position.y == 0.17f) { asset_musuh_1.gerakan = 0; }
            reset_waktu();

        }
    }


    public void balik_badan()
    {
        balik = !balik;
        Vector3 karakter = transform.localScale;
        karakter.x *= -1;
        transform.localScale = karakter;
    }

    public void reset_waktu()
    {
        waktu = 0;
        speed_time = 1;
        seconds = 0;
        dist = 0;
    }
    public void pengurangan_nyawa_musuh_power_amana()
    {
        nilai_nyawa.bar_musuh1.size -= 0.2f;
    }

    public void nilai_power_attack_amana()
    {
        if (power_attack_amana < 100) { power_attack_amana += 50; nilai_power.penambahan_power_amana(); }
    }

    public void palyVoice(AudioClip voice)
    {
        k_set_audio.playsound(voice, audioPlayer);
    }
}
