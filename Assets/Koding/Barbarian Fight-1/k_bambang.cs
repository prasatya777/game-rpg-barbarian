﻿using UnityEngine;
using System.Collections;
using System.Diagnostics;

public class k_bambang : MonoBehaviour {
    public k_musuh_1 asset_musuh_1;
    public k_amana asset_amana;
    public k_alexandria k_utama;
    public k_bar_nyawa nilai_nyawa;
    public k_bar_power nilai_power;
    private AudioSource audioPlayer;
    public int no_karakter_on;
    public Transform[] path;
    public Transform[] path2;
    public float speed = 3.0f;
    public float reachDist = 1.0f;
    public int currentPoint_bambang = 0;
    public int gerakan;

    Animator anim;

    //Variabel Time
    public float waktu;
    public float speed_time = 1;
    public float seconds;
    public float dist;

    //Variabel Balik Badan
    public bool balik;

    //Variabel Indikator Attack
    public int power_attack_bambang;
    public bool indikator_power_bambang;

    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
        audioPlayer = GetComponent<AudioSource>();
        reset_waktu();
        indikator_power_bambang = false;
        power_attack_bambang = 0;
    }

    // Update is called once per frame
    void Update()
    {
        no_karakter_on = k_utama.no_karakter_on;
        if (indikator_power_bambang == true && gerakan == 3)
        { gerakan = 0; }
        if (no_karakter_on == 2 || indikator_power_bambang==true)
        {
            if (gerakan == 0) { langkah_depan_bambang(); }
            else if (gerakan == 1) { pukulan_bambang(); }
            else if (gerakan == 2) { langkah_mundur_bambang(); }

        }

    }

    public void langkah_depan_bambang()
    {
        dist = Vector3.Distance(path[currentPoint_bambang].position, transform.position);
        transform.position = Vector3.MoveTowards(transform.position, path[currentPoint_bambang].position, Time.deltaTime * speed);
        anim.SetBool("lari_bambang", true);
        //print(dist);
        if (transform.position.x == 6.46f && transform.position.y == -1.44f) { gerakan = 1; }
    }

    public void pukulan_bambang()
    {
        waktu += Time.deltaTime * speed_time;
        seconds = waktu % 60;
        if (waktu < 1.3) { anim.SetBool("attack_bambang", true); }
        else 
        {
            nilai_power_attack_bambang();
            k_utama.nilai_power_attack_alexandria();

            if (indikator_power_bambang == true) { asset_musuh_1.life_musuh -= 1000 * 2; pengurangan_nyawa_musuh_power_bambang(); power_attack_bambang = 0; nilai_power.pengurangan_power_bambang(); }
            else { asset_musuh_1.life_musuh -= 1000; nilai_nyawa.pengurangan_nyawa_musuh_1(); }
            anim.SetBool("attack_bambang", false); 
            gerakan = 2;
            balik_badan();
            
        }
    }

    public void langkah_mundur_bambang()
    {
        dist = Vector3.Distance(path2[currentPoint_bambang].position, transform.position);
        transform.position = Vector3.MoveTowards(transform.position, path2[currentPoint_bambang].position, Time.deltaTime * speed);
        //print(dist);
        if (transform.position.x == -11.89f && transform.position.y == 2.02f) 
        { 
            balik_badan(); anim.SetBool("lari_bambang", false); 
            gerakan = 3;
            if (indikator_power_bambang != true || k_utama.no_karakter_on==2)
            { k_utama.no_karakter_on = 3; }
            currentPoint_bambang = 0;
            if (indikator_power_bambang == true) { indikator_power_bambang = false; power_attack_bambang = 0; nilai_power.pengurangan_power_bambang(); }
            if (asset_amana.transform.position.x == -11.44f && asset_amana.transform.position.y == -2.35f) { asset_amana.gerakan = 0; }
            reset_waktu();

        }
    }

    public void balik_badan()
    {
        balik = !balik;
        Vector3 karakter = transform.localScale;
        karakter.x *= -1;
        transform.localScale = karakter;
    }

    public void reset_waktu()
    {
        waktu = 0;
        speed_time = 1;
        seconds = 0;
        dist = 0;
    }

    public void pengurangan_nyawa_musuh_power_bambang()
    {
        nilai_nyawa.bar_musuh1.size -= 0.2f;
    }

    public void nilai_power_attack_bambang()
    {
        if (power_attack_bambang < 100) { power_attack_bambang += 50; nilai_power.penambahan_power_bambang(); }
        else { }
    }

    public void palyVoice(AudioClip voice)
    {
        k_set_audio.playsound(voice, audioPlayer);
    }
}
